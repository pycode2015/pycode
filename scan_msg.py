﻿import re
import json
import requests
from lxml import html



def scan_msg(msg):
    '''
    utility function which accepts ASCII text to scan url and return json payload with mentions, urls and emoticons
    supports ASCI content for mention,emoticon and url. url may point to non ASCI resource
    '''
    if not msg:
        return None
    
    return_map = {}

    URL_REGEX = r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'
    MENTION_REGEX = r"\@(\w+)"
    EMOTICON_REGEX = r"\([A-Za-z]{1,15}\)"

    
    def _scan_url(scanner,token):
        link = [get_url(token)]
        if link:
            if "links" not in return_map:
                return_map["links"] = []
            
            return_map["links"] += link

    def get_url(url):
       
        try:
            pl = requests.get(url)
            page = html.fromstring(pl.text)
            title = page.find(".//title").text
        except:
            title = ""

        if not title:
            return {"url" : url}
        else:
            return { "url" : url, "title": title}
               
    

    def _scan_mention(scanner,token):
        mention = [token[1:]]
        if mention:
            if "mentions" not in return_map:
                return_map["mentions"] = []
                
            return_map["mentions"] += mention
 
    def _scan_emoticon(scanner,token):
        emoticon = [token[1:][:-1]]
        if emoticon:
            
            if "emoticons" not in return_map:
                return_map["emoticons"] = []
                
            return_map["emoticons"] += emoticon

    scanner = re.Scanner([
        (URL_REGEX,_scan_url),
        (MENTION_REGEX,_scan_mention),
        (EMOTICON_REGEX,_scan_emoticon),
        (r".",None)
        ])

  
    
    scanner.scan(msg)
    return json.dumps(return_map,ensure_ascii=False)


if __name__ == "__main__":
    
    print "running sample output"
    print "*********************"

    print(scan_msg("hello world of (aaa) http://www.google.com @nirmal "))
    
    print(scan_msg("Olympics are starting soon; http://www.nbcolympics.com"))
    
    print(scan_msg( "@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016"))

    print(scan_msg( "@chris you around?"))

    print(scan_msg( "Good morning! (megusta) (coffee)"))

    print(scan_msg(" Python http://www.python.org is such a cool language (happyhappyhappy) but not (unhappyunhappyunhappy) by @guido "))

    print(scan_msg(" () @ nothing here http:"))

    print (scan_msg(None))

    print (scan_msg("的中文翻譯 | 英漢字典 http://cdict.net/q/string @nirmal googled for chineese string (gotit) @@的中文翻譯"))

    print (scan_msg("@bbc hk - http://www.bbc.co.uk/zhongwen/simp (unicodehappy)"))

    
