
### What is this repository for? ###

Utility Function to extract mention , emoticon and URL from text and provide as JSON output

### Limitations ###

mentions , urls , emoticons should be ASCII - however urls may point to non-ASCII content

### How do I get set up? ###

Requires Python 2.5 + ( as this uses json ) .

Requires requests , lxml , Please install them by 

pip install requests

pip install lxml 

